# Blog project

[![pipeline status](https://gitlab.com/szogik/blog-project/badges/master/pipeline.svg)](https://gitlab.com/szogik/blog-project/pipelines) [![coverage report](https://gitlab.com/szogik/blog-project/badges/master/coverage.svg)](https://szogik.gitlab.io/blog-project/index.html)

This is an app made for the Project tools course on ELTE Faculty of Informatics.

## Project structure

- The app is a Maven project that consists of two parts: backend and frontend.
  - The frontend is written in Typescript (Angular). The source code can be found in src/main/frontend folder.
  - The backend is written in Java (Spring). The source code can be found in the src/main/java folder.
- The tests can be found in the src/test folder.

## Database structure

![entity relation diagram](docs/blog_ER.jpg)

## The following tools were used during development:

- Java SDK 8+
- Maven 3+
- NodeJS
- npm
- Angular CLI 7+
- A database server (e.g. MySQL, PostgreSQL)
- Git
- IntelliJ Idea and VS Code

### In order to start the project in development mode, you have to do the following

First, connect to a database you want to use for development. Create an application.yml file in your src/main/resources folder based on the sample.yml file.

To run the backend, navigate to the project root in the command line and type

```
./mvnw spring-boot:run
```

The Tomcat server should start up soon.

To run the frontend, navigate to the src/main/frontend folder in the command line. First you have to install dependencies with

```
npm install
```

Then launch the development server with

```
ng serve --open
```

The development server should start up soon and a new browser tab will open with the app.

### If you want to build the project for production

Navigate to the root of the project and type

```
./mvnw package -P production
```

Maven will download the dependencies and build the project for you. The output will be a jar file that you can deploy. It contains both the frontend and the backend with an embedded Tomcat server. The jar file can be found in the target directory.

## Running tests

### Backend

The backend uses the Surefire test framework. In order to run tests, type

```
./mvnw test
```

Code coverage reports can be created with the JaCoCo framework as well. Use the command

```
./mvnw test jacoco:report
```

The reports can be found in the target/site/jacoco folder.

### Frontend

The frontend uses the Karma and Jasmine frameworks for testing. In order to run tests, type

```
ng test
```

A browser is also required. You can set which browser you want to use in the karma.conf.js file located in src/main/frontend/src.
The IstanbulJS framework is used for creating code coverage reports. To create the reports, type

```
ng test --code-coverage
```

The reports can be found in the src/main/frontend/coverage folder.

## Continous integration

There is a CI pipeline configured for the project. Both the backend and frontend tests will be run for every commit on every branch.
If any of the tests fail, a merge request cannot be accepted for that branch. Code coverage reports are created for both backend and frontend tests. You can check the badge in this file for the percentage of code coverage or you can view a detailed report by clicking on it.
The backend code is following the Google Java coding style. The Maven Checkstyle plugin is configured to break the build if there are checkstyle errors.
The frontend uses Prettier to format code on each commit.
Every commit on the master branch also builds the project and packages it into a jar file, which can be downloaded from Gitlab at the CI/CD Pipelines page.

## Rövid ismertető a programról

A jar file futtatása után a localhost:8080 porton érhető el a program. Ilyenkor először a bejelentkező képernyőhöz jutunk. Ha regisztrálva vagyunk, adataink megadásával a login gombra kattintva lehet bejelentkezni, a register gomb pedig a regisztrációs oldalra irányít át.

![login](docs/login.png)

A regisztrációs oldalon a felhasználónevet, email címet és a jelszót kell megadni, illetve tesztelési célzattal itt még kiválasztható, hogy íróként (author) vagy vendégként(olvasóként, guest) akarunk regisztrálni. Sikeres regisztráció esetén visszakerülünk a login képernyőre.

![register](docs/register.png)

Sikeres bejelentkezés után a home képernyő fogad minket, itt a blogon szereplő bejegyzések előnézetei láthatóak, a view post gombra kattintva megnézhetjük a teljes bejegyzést és a hozzá fűzött kommenteket.

![home](docs/home.png)

A bejegyzések alatt szereplő szövegdobozba tudunk hozzászólásokat írni, a korábban írt hozzászólásokat a szövegdoboz alatt láthatjuk.

![view post](docs/view_post.png)

A profil oldalon a felhasználó kommentjei (author esetén bejegyzései is) láthatóak. A gombokra kattintva szintén át lehet navigálni a bejegyzésekre. A bal oldali mezőben a felhasználó adatai láthatóak, amiket az edit gombbal lehet módosítani.

![profile](docs/profile.png)

A módosító űrlapon adhatóak meg az új adatok, majd beküldés után automatikusan frissülnek is.

![edit profile](docs/edit_profile.png)
