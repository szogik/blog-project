package hu.elte.tools.blogbackend;

import static org.assertj.core.api.Assertions.assertThat;

import hu.elte.tools.blogbackend.controllers.AppUserController;
import hu.elte.tools.blogbackend.controllers.CommentController;
import hu.elte.tools.blogbackend.controllers.PostController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class BlogBackendApplicationTests {

  @Autowired private AppUserController userController;

  @Autowired private CommentController commentController;

  @Autowired private PostController postController;

  @Test
  public void contextLoads() {}

  @Test
  public void userControllerLoads() {
    assertThat(userController).isNotNull();
  }

  @Test
  public void commentControllerLoads() {
    assertThat(commentController).isNotNull();
  }

  @Test
  public void postControllerLoads() {
    assertThat(postController).isNotNull();
  }
}
