package hu.elte.tools.blogbackend.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import hu.elte.tools.blogbackend.entities.AppUser;
import hu.elte.tools.blogbackend.entities.Comment;
import hu.elte.tools.blogbackend.entities.Post;
import hu.elte.tools.blogbackend.repositories.AppUserRepository;
import hu.elte.tools.blogbackend.repositories.CommentRepository;
import hu.elte.tools.blogbackend.repositories.PostRepository;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class CommentControllerTest {

  @Autowired protected WebApplicationContext wac;
  @MockBean CommentRepository commentRepository;
  @MockBean PostRepository postRepository;
  @MockBean AppUserRepository appUserRepository;
  Comment comment1;
  Comment comment2;
  AppUser testUser;
  Post testPost;
  List<Comment> allComments;
  @Autowired private MockMvc mockMvc;

  /**
   * Setup method for the comment controller test.
   */
  @Before
  public void setUp() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).apply(springSecurity()).build();

    testUser = new AppUser();
    testUser.setId(100);
    testUser.setUsername("testuser1");
    testUser.setPassword("testpw");

    testPost = new Post();
    testPost.setTitle("testpost");
    testPost.setAuthor(testUser);
    testPost.setContent("testcontent");
    testPost.setPosted(Date.valueOf("2019-01-16"));
    testPost.setId(1);

    comment1 = new Comment();
    comment1.setCommenter(testUser);
    comment1.setContent("testcomment");
    comment1.setId(1);
    comment1.setPost(testPost);
    comment1.setPosted(Date.valueOf("2018-03-05"));

    comment2 = new Comment();
    comment2.setCommenter(testUser);
    comment2.setContent("testcomment2");
    comment2.setId(2);
    comment2.setPost(testPost);
    comment2.setPosted(Date.valueOf("2018-03-06"));
  }

  // ----- GetComments() tests -----
  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void getCommentsTest_multipleComments() throws Exception {
    allComments = new ArrayList();
    allComments.add(comment1);
    allComments.add(comment2);

    when(commentRepository.findAll()).thenReturn(allComments);

    mockMvc
        .perform(get("/comments").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].content", is("testcomment")))
        .andExpect(jsonPath("$[1].content", is("testcomment2")));
  }

  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void getCommentsTest_emptyComments() throws Exception {
    allComments = new ArrayList();

    when(commentRepository.findAll()).thenReturn(allComments);

    mockMvc
        .perform(get("/comments").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(0)));
  }

  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void getCommentsTest_singleComment() throws Exception {
    allComments = new ArrayList();
    allComments.add(comment1);

    when(commentRepository.findAll()).thenReturn(allComments);

    mockMvc
        .perform(get("/comments").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(1)))
        .andExpect(jsonPath("$[0].content", is("testcomment")));
  }

  // ----- getComment() tests -----
  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void getCommentTest_presentComment() throws Exception {
    when(commentRepository.findById(comment1.getId())).thenReturn(Optional.of(comment1));

    mockMvc
        .perform(get("/comments/" + comment1.getId()).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.content", is("testcomment")));
  }

  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void getCommentTest_notPresentComment() throws Exception {
    when(commentRepository.findById(comment1.getId())).thenReturn(Optional.empty());

    mockMvc
        .perform(get("/comments/" + 1).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

  // ----- postComment() tests -----
  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void postCommentTest_SimplePost() throws Exception {
    // parsing into json for the request body
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
    ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
    String requestJson = ow.writeValueAsString(comment1);

    when(appUserRepository.findById(testUser.getId())).thenReturn(Optional.of(testUser));
    when(postRepository.findById(testPost.getId())).thenReturn(Optional.of(testPost));

    mockMvc
        .perform(
            post("/comments")
                .param("post", testPost.getId().toString())
                .param("commenter", testUser.getId().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
        .andExpect(status().isOk());
  }

  // -----putComment() tests-----
  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void putCommentTest_NotExistingComment() throws Exception {
    // parsing into json for the request body
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
    ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
    String requestJson = ow.writeValueAsString(comment1);

    when(commentRepository.findById(comment1.getId())).thenReturn(Optional.empty());

    mockMvc
        .perform(
            put("/comments/" + comment1.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
        .andExpect(status().isNotFound());
  }

  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void putCommentTest_ExistingComment() throws Exception {
    // parsing into json for the request body
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
    ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
    String requestJson = ow.writeValueAsString(comment1);

    when(commentRepository.findById(comment1.getId())).thenReturn(Optional.of(comment1));

    mockMvc
        .perform(
            put("/comments/" + comment1.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
        .andExpect(status().isOk());
  }

  // ----- deleteComment() test -----
  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void deleteCommentTest_PresentComment() throws Exception {

    when(commentRepository.findById(comment1.getId())).thenReturn(Optional.of(comment1));

    mockMvc
        .perform(delete("/comments/" + comment1.getId()).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void deleteCommentTest_NotPresentComment() throws Exception {

    when(commentRepository.findById(comment1.getId())).thenReturn(Optional.empty());

    mockMvc
        .perform(delete("/comments/" + 69).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }
}
