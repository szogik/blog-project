package hu.elte.tools.blogbackend.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import hu.elte.tools.blogbackend.entities.AppUser;
import hu.elte.tools.blogbackend.entities.Post;
import hu.elte.tools.blogbackend.repositories.AppUserRepository;
import hu.elte.tools.blogbackend.repositories.PostRepository;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class PostControllerTest {

  AppUser testUser;
  Post post1;
  Post post2;
  List<Post> allPosts;
  @Autowired private MockMvc mockMvc;
  @Autowired private PostController postController;
  @MockBean private PostRepository postRepository;
  @MockBean private AppUserRepository appUserRepository;

  /**
   * Setup method for the post controller test.
   */
  @Before
  public void setUp() {

    testUser = new AppUser();
    testUser.setId(100);
    testUser.setUsername("testuser1");
    testUser.setPassword("testpw");

    post1 = new Post();
    post1.setTitle("testpost");
    post1.setAuthor(testUser);
    post1.setContent("testcontent");
    post1.setPosted(Date.valueOf("2019-01-16"));
    post1.setId(1);

    post2 = new Post();
    post2.setTitle("testpost2");
    post2.setAuthor(testUser);
    post2.setContent("testcontent2");
    post2.setPosted(Date.valueOf("2019-01-16"));
    post2.setId(2);
  }

  // ----- GetPosts() tests -----
  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void getPostsTest_multiplePosts() throws Exception {
    ArrayList<Post> allPosts = new ArrayList();
    allPosts.add(post1);
    allPosts.add(post2);

    when(postRepository.findAll()).thenReturn(allPosts);

    mockMvc
        .perform(get("/posts").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].content", is("testcontent")))
        .andExpect(jsonPath("$[1].content", is("testcontent2")));
  }

  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void getPostsTest_emptyUsers() throws Exception {
    ArrayList<Post> allPosts = new ArrayList();

    when(postRepository.findAll()).thenReturn(allPosts);

    mockMvc
        .perform(get("/posts").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(0)));
  }

  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void getPostsTest_singleUser() throws Exception {
    ArrayList<Post> allPosts = new ArrayList();
    allPosts.add(post1);

    when(postRepository.findAll()).thenReturn(allPosts);

    mockMvc
        .perform(get("/posts").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(1)))
        .andExpect(jsonPath("$[0].content", is("testcontent")));
  }

  // ----- getPost() tests -----
  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void getPostTest_presentPost() throws Exception {
    when(postRepository.findById(post1.getId())).thenReturn(Optional.of(post1));

    mockMvc
        .perform(get("/posts/" + post1.getId()).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void getPostTest_notPresentPost() throws Exception {
    when(postRepository.findById(post1.getId())).thenReturn(Optional.empty());

    mockMvc
        .perform(get("/posts/" + 1).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

  // ----- postPost() tests -----
  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void postPostTest_SimplePost() throws Exception {
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
    ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
    String requestJson = ow.writeValueAsString(post1);

    when(appUserRepository.findById(testUser.getId())).thenReturn(Optional.of(testUser));

    mockMvc
        .perform(
            post("/posts")
                .param("author", testUser.getId().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
        .andExpect(status().isOk());
  }

  // -----putPost() tests-----
  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void putPostTest_NotExistingPost() throws Exception {
    // parsing into json for the request body
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
    ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
    String requestJson = ow.writeValueAsString(post1);

    when(postRepository.findById(post1.getId())).thenReturn(Optional.empty());

    mockMvc
        .perform(
            put("/posts/" + post1.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
        .andExpect(status().isNotFound());
  }

  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void putPostTest_ExistingPost() throws Exception {
    // parsing into json for the request body
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
    ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
    String requestJson = ow.writeValueAsString(post1);

    when(postRepository.findById(post1.getId())).thenReturn(Optional.of(post1));

    mockMvc
        .perform(
            put("/posts/" + post1.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
        .andExpect(status().isOk());
  }

  // ----- deletePost() test -----
  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void deletePostTest_PresentPost() throws Exception {

    when(postRepository.findById(post1.getId())).thenReturn(Optional.of(post1));

    mockMvc
        .perform(delete("/posts/" + post1.getId()).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void deletePostTest_NotPresentPost() throws Exception {

    when(postRepository.findById(post1.getId())).thenReturn(Optional.empty());

    mockMvc
        .perform(delete("/posts/" + 69).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }
}
