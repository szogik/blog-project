package hu.elte.tools.blogbackend.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import hu.elte.tools.blogbackend.entities.AppUser;
import hu.elte.tools.blogbackend.repositories.AppUserRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class AppUserControllerTest {

  @Autowired protected WebApplicationContext wac;
  @Autowired AppUserController userController;
  @MockBean AppUserRepository userRepository;
  AppUser requester;
  AppUser user1;
  AppUser user2;
  List<AppUser> allUsers;
  private MockMvc mockMvc;

  /**
   * Setup method for the user controller test.
   */
  @Before
  public void setUp() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).apply(springSecurity()).build();

    requester = new AppUser();
    requester.setId(99);
    requester.setUsername("testuser1");
    requester.setPassword("testpw");
    requester.setRole(AppUser.Role.ROLE_AUTHOR);

    user1 = new AppUser();
    user1.setId(100);
    user1.setUsername("testuser1");
    user1.setPassword("testpw");
    user1.setRole(AppUser.Role.ROLE_AUTHOR);
    user2 = new AppUser();
    user2.setId(101);
    user2.setUsername("testuser2");
    user2.setPassword("testpw");
    user2.setRole(AppUser.Role.ROLE_AUTHOR);
  }

  // ------UserController.getUsers() tests-----
  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void getUsersTest_multipleUsers() throws Exception {
    allUsers = new ArrayList();
    allUsers.add(user1);
    allUsers.add(user2);

    when(userRepository.findAll()).thenReturn(allUsers);

    mockMvc
        .perform(
            get("/users") // .with(user("user").roles("ROLE_AUTHOR"))
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].username", is("testuser1")))
        .andExpect(jsonPath("$[1].username", is("testuser2")));
  }

  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void getUsersTest_emptyUsers() throws Exception {
    allUsers = new ArrayList();

    when(userRepository.findAll()).thenReturn(allUsers);

    mockMvc
        .perform(get("/users").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(0)));
  }

  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void getUsersTest_singleUser() throws Exception {
    allUsers = new ArrayList();
    allUsers.add(user1);

    when(userRepository.findAll()).thenReturn(allUsers);

    mockMvc
        .perform(get("/users").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(1)))
        .andExpect(jsonPath("$[0].username", is("testuser1")));
  }

  // -----UserController.getUser() tests-----
  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void getUserTest_presentUser() throws Exception {

    when(userRepository.findById(user1.getId())).thenReturn(Optional.of(user1));

    mockMvc
        .perform(get("/users/" + user1.getId()).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.username", is("testuser1")));
  }

  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void getUserTest_notPresentUser() throws Exception {
    when(userRepository.findById(69)).thenReturn(Optional.empty());

    mockMvc
        .perform(get("/users/" + 69).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }
  /*
   * //----- register() tests ----- http 405 Method not allowed?
   *
   * @Test
   *
   * @WithMockUser(username="admin",roles={"AUTHOR"}) public void
   * postUserTest_SimplePost() throws Exception{ //parsing into json for the request
   * body ObjectMapper mapper = new ObjectMapper();
   * mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false); ObjectWriter ow =
   * mapper.writer().withDefaultPrettyPrinter(); String
   * requestJson=ow.writeValueAsString(user1);
   *
   * when(userRepository.findByUsername(user1.getUsername())).thenReturn(Optional.of(
   * user1));
   *
   * mockMvc.perform(post("/users") .contentType(MediaType.APPLICATION_JSON)
   * .content(requestJson)) .andExpect(status().isOk()); }
   */

  // -----putUser() tests-----
  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void putUserTest_ExistingUser() throws Exception {
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
    ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
    String requestJson = ow.writeValueAsString(user1);

    when(userRepository.findById(user1.getId())).thenReturn(Optional.of(user1));

    mockMvc
        .perform(
            put("/users/" + user1.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
        .andExpect(status().isOk());
  }

  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void putUserTest_NotExistingUser() throws Exception {
    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
    ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
    String requestJson = ow.writeValueAsString(user1);

    when(userRepository.findById(user1.getId())).thenReturn(Optional.empty());

    mockMvc
        .perform(
            put("/users/" + user1.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
        .andExpect(status().isNotFound());
  }

  // ----- deleteUser() test -----
  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void deleteUserTest_PresentUser() throws Exception {

    when(userRepository.findById(user1.getId())).thenReturn(Optional.of(user1));

    mockMvc
        .perform(delete("/users/" + user1.getId()).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  @WithMockUser(
      username = "admin",
      roles = {"AUTHOR"})
  public void deleteUserTest_NotPresentUser() throws Exception {
    when(userRepository.findById(user1.getId())).thenReturn(Optional.empty());

    mockMvc
        .perform(delete("/users/" + user1.getId()).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }
}
