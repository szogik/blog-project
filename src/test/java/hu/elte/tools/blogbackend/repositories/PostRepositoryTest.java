package hu.elte.tools.blogbackend.repositories;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

import hu.elte.tools.blogbackend.entities.AppUser;
import hu.elte.tools.blogbackend.entities.Post;
import java.sql.Date;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class PostRepositoryTest {

  private static AppUser user1;
  private static AppUser user2;
  private static Post post1;
  private static Post post2;
  @Autowired private TestEntityManager entityManager;
  @Autowired private PostRepository postRepository;

  /**
   * Setup method for post repository test.
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {

    user1 = new AppUser();
    user1.setUsername("testuser1");
    user1.setPassword("testpw");
    user1.setRole(AppUser.Role.ROLE_AUTHOR);
    user1.setEmail("test@test.com");
    entityManager.persistAndFlush(user1);

    user2 = new AppUser();
    user2.setUsername("testuser2");
    user2.setPassword("testpw");
    user2.setRole(AppUser.Role.ROLE_AUTHOR);
    user2.setEmail("test@test.com");
    entityManager.persistAndFlush(user2);

    post1 = new Post();
    post1.setTitle("testpost1");
    post1.setAuthor(user1);
    post1.setContent("testcontent");
    post1.setPosted(Date.valueOf("2019-01-16"));
    entityManager.persistAndFlush(post1);

    post2 = new Post();
    post2.setTitle("testpost2");
    post2.setAuthor(user1);
    post2.setContent("testcontent");
    post2.setPosted(Date.valueOf("2019-01-16"));
    entityManager.persistAndFlush(post2);
  }

  @Test
  public void whenFindAllByAuthorId() {
    // when
    List<Post> posts = postRepository.findAllByAuthorId(user1.getId());

    // then
    assertThat(posts.size()).isEqualTo(2);
    assertThat(posts.get(0)).isEqualTo(post1);
    assertThat(posts.get(1)).isEqualTo(post2);

    // when
    posts = postRepository.findAllByAuthorId(user2.getId());

    // then
    assertThat(posts.size()).isEqualTo(0);
  }
}
