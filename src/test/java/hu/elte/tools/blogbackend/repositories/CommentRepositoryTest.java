package hu.elte.tools.blogbackend.repositories;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

import hu.elte.tools.blogbackend.entities.AppUser;
import hu.elte.tools.blogbackend.entities.Comment;
import hu.elte.tools.blogbackend.entities.Post;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;



@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class CommentRepositoryTest {

  private static Comment comment1;
  private static Comment comment2;
  private static Comment comment3;
  private static AppUser user1;
  private static AppUser user2;
  private static Post post1;
  private static Post post2;
  @Autowired private TestEntityManager entityManager;
  @Autowired private CommentRepository commentRepository;

  /**
   * Setup method for comment repository test.
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {

    user1 = new AppUser();
    user1.setUsername("testuser1");
    user1.setPassword("testpw");
    user1.setRole(AppUser.Role.ROLE_AUTHOR);
    user1.setEmail("test@test.com");
    entityManager.persistAndFlush(user1);

    user2 = new AppUser();
    user2.setUsername("testuser2");
    user2.setPassword("testpw");
    user2.setRole(AppUser.Role.ROLE_AUTHOR);
    user2.setEmail("test@test.com");
    entityManager.persistAndFlush(user2);

    post1 = new Post();
    post1.setTitle("testpost1");
    post1.setAuthor(user1);
    post1.setContent("testcontent");
    post1.setPosted(Date.valueOf("2019-01-16"));
    entityManager.persistAndFlush(post1);

    post2 = new Post();
    post2.setTitle("testpost2");
    post2.setAuthor(user1);
    post2.setContent("testcontent");
    post2.setPosted(Date.valueOf("2019-01-16"));
    entityManager.persistAndFlush(post2);

    comment1 = new Comment();
    comment1.setCommenter(user1);
    comment1.setContent("testcomment1");
    comment1.setPost(post1);
    comment1.setPosted(Date.valueOf("2018-03-05"));
    entityManager.persistAndFlush(comment1);

    comment2 = new Comment();
    comment2.setCommenter(user2);
    comment2.setContent("testcomment2");
    comment2.setPost(post1);
    comment2.setPosted(Date.valueOf("2018-03-06"));
    entityManager.persistAndFlush(comment2);

    comment3 = new Comment();
    comment3.setCommenter(user2);
    comment3.setContent("testcomment3");
    comment3.setPost(post2);
    comment3.setPosted(Date.valueOf("2018-03-06"));
    entityManager.persistAndFlush(comment3);

    ArrayList<Comment> post1Comments = new ArrayList();
    post1Comments.add(comment1);
    post1Comments.add(comment2);
    post1.setComments(post1Comments);
    ArrayList<Comment> post2Comments = new ArrayList();
    post2Comments.add(comment3);
    post2.setComments(post2Comments);
  }

  @Test
  public void whenFindAllByCommenterId() {
    // when
    List<Comment> comments = commentRepository.findAllByCommenterId(user1.getId());

    // then
    assertThat(comments.size()).isEqualTo(1);
    assertThat(comments.get(0)).isEqualTo(comment1);

    // when
    comments = commentRepository.findAllByCommenterId(user2.getId());

    // then
    assertThat(comments.size()).isEqualTo(2);
    assertThat(comments.get(0)).isEqualTo(comment2);
    assertThat(comments.get(1)).isEqualTo(comment3);
  }

  @Test
  public void whenFindAllByPostId() {
    // when
    List<Comment> comments = commentRepository.findAllByPostId(post1.getId());

    // then
    assertThat(comments.size()).isEqualTo(2);
    assertThat(comments.get(0)).isEqualTo(comment1);
    assertThat(comments.get(1)).isEqualTo(comment2);

    // when
    comments = commentRepository.findAllByPostId(post2.getId());

    // then
    assertThat(comments.size()).isEqualTo(1);
    assertThat(comments.get(0)).isEqualTo(comment3);
  }

  @Test
  public void whenFindAllByCommenterIdAndPostId() {
    // when
    List<Comment> comments =
        commentRepository.findAllByCommenterIdAndPostId(user1.getId(), post1.getId());

    // then
    assertThat(comments.size()).isEqualTo(1);
    assertThat(comments.get(0)).isEqualTo(comment1);

    // when
    comments = commentRepository.findAllByCommenterIdAndPostId(user2.getId(), post1.getId());

    // then
    assertThat(comments.size()).isEqualTo(1);
    assertThat(comments.get(0)).isEqualTo(comment2);

    // when
    comments = commentRepository.findAllByCommenterIdAndPostId(user1.getId(), post2.getId());

    // then
    assertThat(comments.size()).isEqualTo(0);

    // when
    comments = commentRepository.findAllByCommenterIdAndPostId(user2.getId(), post2.getId());

    // then
    assertThat(comments.size()).isEqualTo(1);
    assertThat(comments.get(0)).isEqualTo(comment3);
  }
}
