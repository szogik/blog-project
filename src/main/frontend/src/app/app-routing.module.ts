import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./pages/login/login.component";
import { RegisterComponent } from "./pages/register/register.component";
import { HomeComponent } from "./pages/home/home.component";
import { ProfileComponent } from './pages/profile/profile.component';
import { EditPageComponent } from './pages/edit-page/edit-page.component';
import { PostDetailsComponent } from './pages/post-details/post-details.component';
import { CreatePostComponent } from './pages/create-post/create-post.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
	{ path: "login", component: LoginComponent },
	{ path: "register", component: RegisterComponent },
	{ path: "home", component: HomeComponent },
	{ path: "profile", component: ProfileComponent },
	{ path: "edit", component: EditPageComponent },
	{ path: "posts/:id", component: PostDetailsComponent },
	{
		path: "createpost", component: CreatePostComponent, canActivate: [AuthGuard],
		data: {
			roles: ['ROLE_AUTHOR']
		}
	},
	{ path: "", redirectTo: "/home", pathMatch: "full" }
];

@NgModule({
	imports: [RouterModule.forRoot(routes), RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
	exports: [RouterModule]
})
export class AppRoutingModule { }
