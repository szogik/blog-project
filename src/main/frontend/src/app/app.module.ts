import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LoginComponent } from "./pages/login/login.component";
import { RegisterComponent } from "./pages/register/register.component";
import { HttpClientModule } from "@angular/common/http";
import { HomeComponent } from "./pages/home/home.component";
import { CommonModule } from "@angular/common";
import { ProfileComponent } from "./pages/profile/profile.component";
import { ProfileInformationComponent } from "./pages/profile-information/profile-information.component";
import { ProfileCommentsComponent } from "./pages/profile-comments/profile-comments.component";
import { EditPageComponent } from "./pages/edit-page/edit-page.component";
import { EditPageFormComponent } from "./pages/edit-page-form/edit-page-form.component";
import { LocationStrategy, HashLocationStrategy } from "@angular/common";
import { PostDetailsComponent } from "./pages/post-details/post-details.component";
import { CreatePostComponent } from "./pages/create-post/create-post.component";
import { NgxPaginationModule } from "ngx-pagination";
import { ProfilePostComponent } from "./pages/profile-post/profile-post.component";
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProfileComponent,
    ProfileInformationComponent,
    ProfileCommentsComponent,
    EditPageComponent,
    EditPageFormComponent,
    PostDetailsComponent,
    CreatePostComponent,
    ProfilePostComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
