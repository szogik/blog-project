import { Component } from "@angular/core";
import { AuthService, httpOptions } from "./services/auth-service/auth.service";
import { LocalDataService } from "./services/loca-data-service/local-data.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "blog-frontend";

  private authService: AuthService;

  constructor(
    authService: AuthService,
    private localStorage: LocalDataService
  ) {
    this.authService = authService;
  }

  ngOnInit() {
    this.nitializeApp();
  }

  nitializeApp() {
    if (this.localStorage.getToken() && this.localStorage.getUserData()) {
      this.authService.setUser(this.localStorage.getUserData());
      this.authService.setToken(this.localStorage.getToken().replace(/"/g, ""));
    }
  }
}
