import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/services/post-service/post.service';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { Post } from '../classes/post/post';

@Component({
  selector: 'app-profile-post',
  templateUrl: './profile-post.component.html',
  styleUrls: ['./profile-post.component.css']
})
export class ProfilePostComponent implements OnInit {

  constructor(private postService: PostService,
    private authService : AuthService) { }
    comment : Comment[];
    post : Post[];
    clicked=false;
    async ngOnInit()  {
      this.post = await this.postService.getPostsByUser(this.authService.user.id);
    }

    handleClick(event: Event, id : number) {
      this.clicked=!this.clicked;
      if(this.clicked){
        var x = document.getElementById(id.toString()).innerHTML;
        document.getElementById(id.toString()).innerHTML= this.post[id-1].content.toString();
        document.getElementById(id.toString()+"button").innerHTML="Less";
      }else{
        var x = document.getElementById(id.toString()).innerHTML;
        document.getElementById(id.toString()).innerHTML= this.post[id-1].content.slice(0,400)+"...";
        document.getElementById(id.toString()+"button").innerHTML="More";
      }
    }
}
