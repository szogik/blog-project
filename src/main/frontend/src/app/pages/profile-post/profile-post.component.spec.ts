import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MockComponent } from 'ng-mocks';
import { ProfilePostComponent } from './profile-post.component';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { Pipe, PipeTransform } from '@angular/core';
import { Post } from '../classes/post/post';
import { PostService } from 'src/app/services/post-service/post.service';
import { User } from '../classes/user/user';

describe('ProfilePostComponent', () => {
  let component: ProfilePostComponent;
  let fixture: ComponentFixture<ProfilePostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
         ProfilePostComponent,
         MockPipe
      ],
      imports: [
        NgxPaginationModule
      ],
      providers: [
        { provide: AuthService, useClass: MockAuthService },
        { provide: PostService, useClass: MockPostService}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilePostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
class MockAuthService {

  user: User = new User("Kenny", "asd", "ROLE_AUTHOR", "anemail@email.email");

  public isAddmin(){
    return true;
  }
}

class MockPostService {

  public getPostsByUser(){
    return new Array<Post>();
  }
}


@Pipe({name: 'paginate'})
class MockPipe implements PipeTransform {
    transform(value: number): number {
        return value;
    }
}