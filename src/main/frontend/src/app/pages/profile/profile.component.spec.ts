import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileComponent } from './profile.component';
import { MockComponent, MockDirective } from 'ng-mocks';
import { ProfileInformationComponent } from '../profile-information/profile-information.component';
import { ProfileCommentsComponent } from '../profile-comments/profile-comments.component';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        ProfileComponent,
        MockComponent(ProfileInformationComponent),
        MockComponent(ProfileCommentsComponent)
       ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
