import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostService } from 'src/app/services/post-service/post.service';
import { CommentService } from 'src/app/services/comment-service/comment.service';
import { Post } from 'src/app/pages/classes/post/post';
import { Comment } from 'src/app/pages/classes/comment/comment';
import { Observable, interval } from 'rxjs';
import { flatMap } from 'rxjs/operators'

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {

  public id : number;
  comments : Comment [];
  post : Post;
  content: String;

  constructor(private route: ActivatedRoute, private postService : PostService, 
    private commentService : CommentService) {
      this.post = new Post();
   }

  async ngOnInit() {
    this.id = parseInt(this.route.snapshot.paramMap.get('id'));
    this.post = await this.postService.getPost(this.id);
    this.comments = await this.commentService.getCommentsByPost(this.id);

    interval(60000).pipe(
      flatMap(async () => await this.commentService.getCommentsByPost(this.id))).
        subscribe(data => this.comments = data);
  }

  async submitComment() {
    await this.commentService.postComment(this.content, this.id);
    this.comments = await this.commentService.getCommentsByPost(this.id);
    this.content = "";
  }

}
