import { Component, OnInit } from '@angular/core';
import { AuthService } from "src/app/services/auth-service/auth.service";

@Component({
  selector: 'app-profile-information',
  templateUrl: './profile-information.component.html',
  styleUrls: ['./profile-information.component.css'] 
})
export class ProfileInformationComponent implements OnInit {

  email: String;
  role: String;
  gender : String = "";
  website: String = "";
  birth: String = "";
  company: String = "";

  constructor(private authService: AuthService) {
    this.email = authService.user.email;
    this.role = authService.user.role;
    this.website = authService.user.website;
    this.birth = authService.user.birth;
    this.company = authService.user.company;
    this.gender = authService.user.gender;
   }

  ngOnInit() {
    this.authService.formSubmitted.subscribe((value) => { 
      this.email = value[0];
      this.role = value[1];
      this.gender = value[2];
      this.website = value[3];
      this.birth = value[4];
      this.company = value[5];
    });
  }

}
