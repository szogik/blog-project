import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Subject } from 'rxjs';
import { ProfileInformationComponent } from './profile-information.component';
import { User } from '../classes/user/user';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('ProfileInformationComponent', () => {
  let component: ProfileInformationComponent;
  let fixture: ComponentFixture<ProfileInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileInformationComponent],
      imports: [
        RouterTestingModule
      ],
      providers: [
        { provide: AuthService, useClass: MockAuthService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ProfileInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', async(() => {
    expect(component).toBeTruthy();
  }));
});

class MockAuthService {
  user: User = new User("Kenny", "asd", "ROLE_AUTHOR", "anemail@email.email");

  formSubmitted : Subject<String[]> = new Subject<String[]>();

  public isLoggedIn(){
    return true;
  }
};