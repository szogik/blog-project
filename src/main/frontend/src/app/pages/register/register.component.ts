import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth-service/auth.service';

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  constructor(private fb: FormBuilder,private router: Router,private authService: AuthService) {
    this.registerForm = this.fb.group({
      username: ["", Validators.required],
      email: ["", Validators.required],
	  password: ["", Validators.required],
	  role: ["", Validators.required]
    });
  }

  ngOnInit() {}

  register() {
    if (this.registerForm.invalid) {
      console.log("hiba");
      return;
  	}
	//console.log("register");
	console.log(this.registerForm.value.username);

	//REGISTER HERE
	this.authService.register(this.registerForm.value.username,this.registerForm.value.password,this.registerForm.value.email,this.registerForm.value.role).then((response) =>{
		if(response){
			this.router.navigate(['/login']);
		}
	});
  }
}
