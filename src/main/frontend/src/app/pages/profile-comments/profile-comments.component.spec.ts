import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileCommentsComponent } from './profile-comments.component';
import { HttpClientModule } from '@angular/common/http';
import { User } from '../classes/user/user';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { MockComponent } from 'ng-mocks';
import { ProfilePostComponent } from '../profile-post/profile-post.component';
import { Pipe, PipeTransform } from '@angular/core';
import { NgxPaginationModule } from 'ngx-pagination';
import { CommentService } from 'src/app/services/comment-service/comment.service';

describe('ProfileCommentsComponent', () => {
  let component: ProfileCommentsComponent;
  let fixture: ComponentFixture<ProfileCommentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
         ProfileCommentsComponent,
         MockComponent(ProfilePostComponent),
         MockPipe
      ],
      imports: [
        HttpClientModule,
        NgxPaginationModule
      ],
      providers: [
        { provide: AuthService, useClass: MockAuthService },
        { provide: CommentService, useClass: MockCommentService}
      ]
      
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

class MockAuthService {

  user: User = new User("Kenny", "asd", "ROLE_AUTHOR", "anemail@email.email");
  
  public isAddmin(){
    return true;
  }
};

class MockCommentService {
  public getComments(){
    return new Array<Comment>();
  }

}

@Pipe({name: 'paginate'})
class MockPipe implements PipeTransform {
    transform(value: number): number {
        return value;
    }
}