import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { CommentService } from 'src/app/services/comment-service/comment.service';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import {Comment} from '../../pages/classes/comment/comment';

@Component({
  selector: 'app-profile-comments',
  templateUrl: './profile-comments.component.html',
  styleUrls: ['./profile-comments.component.css']
})
export class ProfileCommentsComponent implements OnInit {
  
  @Input() private comments: Observable<[]>;
  constructor(private commentService : CommentService,
    private authService : AuthService) { }
    comment : Comment[];
    clicked=false;
    async ngOnInit()  {
      this.comment = await this.commentService.getComments(this.authService.user.id);
  }
  handleClick(event: Event, id : number) {
    this.clicked=!this.clicked;
    if(this.clicked){
      var x = document.getElementById(id.toString()).innerHTML;
      document.getElementById(id.toString()).innerHTML= this.comment[id-1].content;
      document.getElementById(id.toString()+"button").innerHTML="Less";
    }else{
      var x = document.getElementById(id.toString()).innerHTML;
      document.getElementById(id.toString()).innerHTML= this.comment[id-1].content.slice(0,50)+"...";
      document.getElementById(id.toString()+"button").innerHTML="More";
    }
  }
  
}
