import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPageFormComponent } from './edit-page-form.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { User } from '../classes/user/user';

describe('EditPageFormComponent', () => {
  let component: EditPageFormComponent;
  let fixture: ComponentFixture<EditPageFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        EditPageFormComponent
      ],
      imports: [
        HttpClientModule,
        ReactiveFormsModule,
        RouterTestingModule
      ],
      providers : [
        {provide: AuthService, useClass: MockAuthService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(EditPageFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', async(() => {
    expect(component).toBeTruthy();
  }));

});

class MockAuthService{
  user : User = new User("Kenny","asd","ROLE_AUTHOR","anemail@email.email");
};
