import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, RouterModule } from '@angular/router';


@Component({
  selector: 'app-edit-page-form',
  templateUrl: './edit-page-form.component.html',
  styleUrls: ['./edit-page-form.component.css']
})
export class EditPageFormComponent implements OnInit {
  editForm : FormGroup;
  email: string;
  role:string;
  website:string;
  company:string;
  gender:string;
  birth:string;
  
  constructor(private fb: FormBuilder,private router: Router,private authService: AuthService) {     
    this.editForm = this.fb.group({
      id:[],
      email: [],
    role: [],
    website: [],
    company: [],
    gender: [],
    birth:[]
    });
  }

  ngOnInit() {
  }

  edit() {
    if (this.editForm.invalid) {
      console.log("hiba");
      return;
  	}
    
    if(this.editForm.value.email==null){
      this.email=this.authService.user.email;
    }else{
      this.email=this.editForm.value.email;
    }
    if(this.editForm.value.role==null){
      this.role=this.authService.user.role;
    }else{
      this.role=this.editForm.value.role;
    }
    if(this.editForm.value.company==null){
      this.company=this.authService.user.company;
    }else{
      this.company=this.editForm.value.company;
    }
    if(this.editForm.value.website==null){
      this.website=this.authService.user.website;
    }else{
      this.website=this.editForm.value.website;
    }
    if(this.editForm.value.gender==null){
      this.gender=this.authService.user.gender;
    }else{
      this.gender=this.editForm.value.gender;
    }
    if(this.editForm.value.birth==null){
      this.birth=this.authService.user.birth;
    }else{
      this.birth=this.editForm.value.birth;
    }
	//EDIT HERE
  this.authService.edit(this.authService.user.id,this.authService.user.username,
    this.authService.user.password,
    this.email,
    this.role,
    this.website,
    this.gender,
    this.birth,
    this.company
    )
    console.log(this.company)
    console.log("edit page : " + this.authService.user.username);
  }
}
