import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { HttpClientModule } from '@angular/common/http';
import { MockComponent } from 'ng-mocks';
import { LoginComponent } from '../login/login.component';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { User } from '../classes/user/user';
import { FormsModule } from '@angular/forms';
import { Pipe, PipeTransform } from '@angular/core';
import { NgxPaginationModule } from 'ngx-pagination';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        MockComponent(LoginComponent),
        MockPipe
      ],
      imports: [
        HttpClientModule,
        NgxPaginationModule
      ],
      providers: [
        { provide: AuthService, useClass: MockAuthService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', async(() => {
    expect(component).toBeTruthy();
  }));

  class MockAuthService {
    user: User = new User("Kenny", "asd", "ROLE_AUTHOR", "anemail@email.email");
  
    public isLoggedIn(){
      return true;
    }
  };

});


@Pipe({name: 'paginate'})
class MockPipe implements PipeTransform {
    transform(value: number): number {
        
        return value;
    }
}