import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { PostService } from 'src/app/services/post-service/post.service';
import { Post } from 'src/app/pages/classes/post/post';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private authService: AuthService;

  posts : Post[];

  constructor(authService: AuthService, private postService : PostService) {
    this.authService = authService;
  }

  async ngOnInit() {
    this.posts = await this.postService.getPosts();
  }

}
