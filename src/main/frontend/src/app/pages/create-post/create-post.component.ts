import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PostService } from 'src/app/services/post-service/post.service';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-create-post',
	templateUrl: './create-post.component.html',
	styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {
	submitted = false;
	postForm: FormGroup;
	constructor(private formBuilder: FormBuilder,private postService: PostService,private authService: AuthService) {
		this.postForm = this.formBuilder.group({
			title: ["",Validators.required],
			content: ["",Validators.required],
		});
	 }

	ngOnInit() {
	}


	sendPost() {
		this.submitted = true;
		if(this.postForm.invalid){
			console.log("invalid");
			
			return;
		}
		this.postService.sendPost(this.postForm.value.title,this.postForm.value.content,this.authService.user.id).then(response =>{
			console.log(response);
		});
		//It would be nice to have something like this here:
		//this.router.navigate(['/home']);
	}

}
