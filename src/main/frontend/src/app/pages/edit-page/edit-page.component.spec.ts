import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MockComponent, MockRender } from 'ng-mocks';

import { EditPageComponent } from './edit-page.component';
import { HttpClientModule } from '@angular/common/http';
import { ProfileInformationComponent } from '../profile-information/profile-information.component';
import { EditPageFormComponent } from '../edit-page-form/edit-page-form.component';
import { By } from 'protractor';

describe('EditPageComponent', () => {
  let component: EditPageComponent;
  let fixture: ComponentFixture<EditPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        EditPageComponent,
        MockComponent(ProfileInformationComponent),
        MockComponent(EditPageFormComponent)
      ],
      imports: [
        HttpClientModule
      ],
    })
      .compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(EditPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', async(() => {
    expect(component).toBeTruthy();
  }));

  //Valami hatalmas error, elvileg egy beimportált componensben kéne megkeresni, hogy
  //tényleg renderelődik e X html rész
  /*it('should render something inside of the EditPageFormComponent', async(() => {
    const localFixture = MockRender(`
    <app-edit-page-form>
      <label for="username">Username</label>
    </app-edit-page-form>
    `);
    // because component does not have any @ContentChild we can access html directly.
    // assert on some side effect
    const mockedNgContent = localFixture.debugElement
      .query(By.directive(EditPageFormComponent))
      .nativeElement.innerHTML;
    expect(mockedNgContent).toContain('<label for="username">Username</label>');
  }));*/
});
