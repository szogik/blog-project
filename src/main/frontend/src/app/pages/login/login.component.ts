import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth-service/auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) {
    this.loginForm = this.formBuilder.group({
      username: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  get f() {
    return this.loginForm.controls;
  }

  ngOnInit() {}

  login() {
    if (this.loginForm.invalid) {
      return;
    }
    console.log("login");
    //TODO Login
    console.log(this.loginForm.value);
    this.authService
      .login(this.loginForm.value.username, this.loginForm.value.password)
      .then(response => {
        this.router.navigate(['/home']);
      });
  }
}
