import { User } from '../user/user';

export class Comment {
    constructor(content:string, posted: Date, role:string, commenter: User){
        this.content = content;
        this.posted = posted;
		this.commenter = commenter;
    }
    id: number;
    content: string;
    posted: Date;
    commenter: User;
}