export class User {
    constructor(username:string, password:string, role:string, email: string){
        this.username = username;
        this.password = password;
		this.role = role;
		this.email = email;
    }
    id: number;
    username: string;
    password: string;
    role: string;
    email: string;
    gender:string;
    website:string;
    company:string;
    birth:string;
}