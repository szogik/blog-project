import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient, HttpResponse } from "@angular/common/http";
import { User } from "../../pages/classes/user/user";
import { Observable } from "rxjs";
import { LocalDataService } from "../loca-data-service/local-data.service";
import { Subject } from 'rxjs';

export const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json",
    "Authorization": ""
  })
};

@Injectable({
  providedIn: "root"
})
export class AuthService {
  user: User = null;
  token: string = null;
  formSubmitted : Subject<String[]> = new Subject<String[]>();
  redirectUrl: string;
  private url = "http://localhost:8080/users";
  onReady: Observable<any>;
  constructor(
    private http: HttpClient,
    private localStorage: LocalDataService
  ) {}

  async register(
    username: string,
    password: string,
    email: string,
    role: string
  ) {
    try {
      await this.http
        .post(
          `${this.url}/register`,
          {
            username: username,
            password: password,
            email: email,
            role: role
          },
          httpOptions
        )
        .toPromise();
      return Promise.resolve(true);
    } catch (e) {
      console.log("hiba", e);
      return Promise.resolve(false);
    }
  }

  async login(username: string, password: string): Promise<boolean> {
    const token = btoa(`${username}:${password}`);
    httpOptions.headers = httpOptions.headers.set(
      "Authorization",
      `Basic ${token}`
    );
    try {
      const user = await this.http
        .post<User>(`${this.url}/login`, {}, httpOptions)
        .toPromise();
      this.user = user;
      this.localStorage.setUserData(user);
      this.localStorage.setToken(token);
      return Promise.resolve(true);
    } catch (e) {
      console.log("hiba", e);
      return Promise.resolve(false);
    }
  }

  logout() {
    httpOptions.headers = httpOptions.headers.set("Authorization", ``);
    this.user = null;
    this.localStorage.removeToken();
    this.localStorage.removeUserData();
  }

  isLoggedIn() {
    return this.user != null;
  }
  getCurrentUser(): User {
    return this.user;
  }
  async edit(
    id: number,
    username: string,
    password: string,
    email: string,
    role: string,
    website: string,
    gender: string,
    birth: string,
    company: string,
  ) {
    try {
      await this.http
        .put(
          `${this.url}/${this.user.id}`,
          {
            username: username,
            password: password,
            email: email,
            role: role,
            gender: gender,
            company: company,
            website: website,
            birth: birth
          },
          httpOptions
        )
        .toPromise();
      this.formSubmitted.next([email,role,gender,website,birth,company]);
      this.user.email = email
      this.user.role = role
      this.user.gender = gender
      this.user.website = website
      this.user.birth = birth
      this.user.company = company
      this.localStorage.setUserData(this.user)
      return Promise.resolve(true);
    } catch (e) {
      console.log("hiba", e);
      return Promise.resolve(false);
    }
  }

  setUser(user: User) {
	this.user = <any>{};
	this.user = user;
  }
  
  setToken(token: string){
    this.token = token;
    httpOptions.headers = httpOptions.headers.set(
      "Authorization",
      `Basic ${token}`
    );
  }
  isAddmin() {
    return this.user.role==="ROLE_AUTHOR";
  }
}
