import { TestBed, async } from '@angular/core/testing';

import { LocalDataService } from './local-data.service';
import { HttpClientModule } from '@angular/common/http';

describe('LocalDataService', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ]
    }).compileComponents();
  }));

  it('should be created', async(() => {
    const service: LocalDataService = TestBed.get(LocalDataService);
    expect(service).toBeTruthy();
  }));
});
