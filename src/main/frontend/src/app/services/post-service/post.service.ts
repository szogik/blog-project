import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Post } from 'src/app/pages/classes/post/post';
import { httpOptions } from '../auth-service/auth.service';
import { formatDate } from '@angular/common';

@Injectable({
	providedIn: 'root'
})
export class PostService {

	constructor(private http: HttpClient) { }

	async getPost(id: number) {
		return this.http.get<Post>(
			`http://localhost:8080/posts/${id}`,
			httpOptions
		).toPromise().catch((error: HttpErrorResponse) => {
			return null;
		});
	}

  async getPosts(){
    return this.http.get<Post[]>(
      `http://localhost:8080/posts`,      
      httpOptions
    ).toPromise().catch((error:HttpErrorResponse) => {
      return new Array<Post>()
    });
  }
  async getPostsByUser(userID : number){
    return this.http.get<Post[]>(
      `http://localhost:8080/posts/by-user?author=${userID}`,      
      httpOptions
    ).toPromise().catch((error:HttpErrorResponse) => {
      return new Array<Post>()
    });
	}
	async sendPost(title: string, content: string, author: number) {
		const date = formatDate(Date.now(), "yyyy-MM-dd HH:mm:ss", "en-US");
		return this.http.post('http://localhost:8080/posts?author=' + author, {
				"title": title,
				"content": content,
				"posted": date
		}, httpOptions).toPromise();
}

}
