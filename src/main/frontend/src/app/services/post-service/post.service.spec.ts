import { TestBed } from '@angular/core/testing';

import { PostService } from './post.service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

describe('PostService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      FormsModule,
      HttpClientModule
    ],
  }));

  it('should be created', () => {
    const service: PostService = TestBed.get(PostService);
    expect(service).toBeTruthy();
  });
});
