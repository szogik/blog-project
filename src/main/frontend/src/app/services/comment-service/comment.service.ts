import { Injectable } from '@angular/core';
import { formatDate } from '@angular/common';
import { AuthService, httpOptions } from '../auth-service/auth.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {Comment} from '../../pages/classes/comment/comment';
@Injectable({
  providedIn: 'root'
})
export class CommentService {
 
 
  constructor(private http: HttpClient, private authService :AuthService) {}
  
  async getComments(userID : number){
      return this.http.get<Comment[]>(
        `http://localhost:8080/comments/by-user?commenter=${userID}`,      
        httpOptions
      ).toPromise().catch((err: HttpErrorResponse) =>{
        return new Array<Comment>()
      });
  }

  async getCommentsByPost(postID : number){
    return this.http.get<Comment[]>(
      `http://localhost:8080/comments/by-post?post=${postID}`,      
      httpOptions
    ).toPromise().catch((err: HttpErrorResponse) =>{
      return new Array<Comment>()
    });
  }

  async postComment(content : String, post : number) {
    const date = formatDate(Date.now(),"yyyy-MM-dd HH:mm:ss", "en-US");
    return this.http.post<Comment>(
      `http://localhost:8080/comments?commenter=${this.authService.user.id}&post=${post}`,
      {
        "posted" : date,
        "content" : content
      },
      httpOptions
    ).toPromise().catch((err: HttpErrorResponse) =>{
        console.log(err)
    });
  }

}
