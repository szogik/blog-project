import { TestBed, async } from '@angular/core/testing';

import { CommentService } from './comment.service';
import { HttpClientModule } from '@angular/common/http';

describe('CommentService', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ]
    }).compileComponents();
  }));

  it('should be created', async(() => {
    const service: CommentService = TestBed.get(CommentService);
    expect(service).toBeTruthy();
  }));
});
