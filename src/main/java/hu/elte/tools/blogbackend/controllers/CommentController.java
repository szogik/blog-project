package hu.elte.tools.blogbackend.controllers;

import hu.elte.tools.blogbackend.entities.AppUser;
import hu.elte.tools.blogbackend.entities.Comment;
import hu.elte.tools.blogbackend.entities.Post;
import hu.elte.tools.blogbackend.repositories.AppUserRepository;
import hu.elte.tools.blogbackend.repositories.CommentRepository;
import hu.elte.tools.blogbackend.repositories.PostRepository;
import java.util.List;
import java.util.Optional;
import javax.websocket.server.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class for the Comment entity.
 */
@RequestMapping("/comments")
@CrossOrigin
@RestController
public class CommentController {

  @Autowired private CommentRepository commentRepository;

  @Autowired private AppUserRepository userRepository;

  @Autowired private PostRepository postRepository;

  @GetMapping("")
  @Secured({"ROLE_AUTHOR", "ROLE_GUEST"})
  public ResponseEntity<List<Comment>> getComments() {
    List<Comment> comments = commentRepository.findAll();
    return ResponseEntity.ok(comments);
  }

  /**
   * Returns the comments with the provided commenter id if it exists.
   * @param commenter The id of the commenter.
   * @return ResponseEntity
   */
  @GetMapping("/by-user")
  @Secured({"ROLE_AUTHOR", "ROLE_GUEST"})
  public ResponseEntity<List<Comment>> getCommentsByUser(
      @PathParam(value = "commenter") Integer commenter) {
    List<Comment> comments = commentRepository.findAllByCommenterId(commenter);
    return ResponseEntity.ok(comments);
  }

  @GetMapping("/by-post")
  @Secured({"ROLE_AUTHOR", "ROLE_GUEST"})
  public ResponseEntity<List<Comment>> getCommentsByPost(@PathParam(value = "post") Integer post) {
    List<Comment> comments = commentRepository.findAllByPostId(post);
    return ResponseEntity.ok(comments);
  }

  @GetMapping("/by-user-post")
  @Secured({"ROLE_AUTHOR", "ROLE_GUEST"})
  public ResponseEntity<List<Comment>> getPostsByUser(
      @PathParam(value = "commenter") Integer commenter, @PathParam(value = "post") Integer post) {
    List<Comment> comments = commentRepository.findAllByCommenterIdAndPostId(commenter, post);
    return ResponseEntity.ok(comments);
  }

  /**
   * Returns the comment with the provided id if it exists.
   * @param id The id of the comment.
   * @return ResponseEntity
   */
  @GetMapping("/{id}")
  @Secured({"ROLE_AUTHOR", "ROLE_GUEST"})
  public ResponseEntity<Comment> getComment(@PathVariable Integer id) {
    Optional<Comment> optionalComment = commentRepository.findById(id);
    if (optionalComment.isPresent()) {
      return ResponseEntity.ok(optionalComment.get());
    }
    return ResponseEntity.notFound().build();
  }

  /**
   * Creates a comment with the provided data.
   * @param comment The comment data.
   * @param post The id of the post.
   * @param commenter The id of the commenter.
   * @return ResponseEntity
   */
  @PostMapping("")
  @Secured({"ROLE_AUTHOR", "ROLE_GUEST"})
  public ResponseEntity<Comment> postComment(
      @RequestBody Comment comment,
      @PathParam(value = "post") Integer post,
      @PathParam(value = "commenter") Integer commenter) {
    Optional<AppUser> optionalUser = userRepository.findById(commenter);
    Optional<Post> optionalPost = postRepository.findById(post);
    if (optionalPost.isPresent() && optionalUser.isPresent()) {
      comment.setCommenter(optionalUser.get());
      comment.setPost(optionalPost.get());
      return ResponseEntity.ok(commentRepository.save(comment));
    }
    return ResponseEntity.notFound().build();
  }

  /**
   * Updates the comment with the provided id if it exists.
   * @param comment The comment data.
   * @param id The id of the comment.
   * @return ResponseEntity
   */
  @PutMapping("/{id}")
  @Secured({"ROLE_AUTHOR", "ROLE_GUEST"})
  public ResponseEntity<Comment> putComment(
      @RequestBody Comment comment, @PathVariable Integer id) {
    Optional<Comment> optionalComment = commentRepository.findById(id);
    if (optionalComment.isPresent()) {
      Comment oldComment = optionalComment.get();
      comment.setId(oldComment.getId());
      comment.setCommenter(oldComment.getCommenter());
      comment.setPost(oldComment.getPost());
      return ResponseEntity.ok(commentRepository.save(comment));
    }
    return ResponseEntity.notFound().build();
  }

  /**
   * Deletes the comment with the provided id if it exists.
   * @param id The id of the comment.
   * @return ResponseEntity
   */
  @DeleteMapping("/{id}")
  @Secured({"ROLE_AUTHOR", "ROLE_GUEST"})
  public ResponseEntity deleteComment(@PathVariable Integer id) {
    Optional<Comment> optionalComment = commentRepository.findById(id);
    if (optionalComment.isPresent()) {
      commentRepository.delete(optionalComment.get());
      return ResponseEntity.ok().build();
    }
    return ResponseEntity.notFound().build();
  }
}
