package hu.elte.tools.blogbackend.controllers;

import hu.elte.tools.blogbackend.entities.AppUser;
import hu.elte.tools.blogbackend.entities.Post;
import hu.elte.tools.blogbackend.repositories.AppUserRepository;
import hu.elte.tools.blogbackend.repositories.PostRepository;
import java.util.List;
import java.util.Optional;
import javax.websocket.server.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * Controller class for the Post entity.
 */
@RequestMapping("/posts")
@CrossOrigin
@RestController
public class PostController {

  @Autowired private PostRepository postRepository;

  @Autowired private AppUserRepository userRepository;

  @GetMapping("")
  @Secured({"ROLE_AUTHOR", "ROLE_GUEST"})
  public ResponseEntity<List<Post>> getPosts() {
    List<Post> posts = postRepository.findAll();
    return ResponseEntity.ok(posts);
  }

  @GetMapping("/by-user")
  @Secured({"ROLE_AUTHOR", "ROLE_GUEST"})
  public ResponseEntity<List<Post>> getPostsByUser(@PathParam(value = "author") Integer author) {
    List<Post> posts = postRepository.findAllByAuthorId(author);
    return ResponseEntity.ok(posts);
  }

  /**
   * Returns the post with the provided id if it exists.
   * @param id The id of the post.
   * @return ResponseEntity
   */
  @GetMapping("/{id}")
  @Secured({"ROLE_AUTHOR", "ROLE_GUEST"})
  public ResponseEntity<Post> getPost(@PathVariable Integer id) {
    Optional<Post> optionalPost = postRepository.findById(id);
    if (optionalPost.isPresent()) {
      return ResponseEntity.ok(optionalPost.get());
    }
    return ResponseEntity.notFound().build();
  }

  /**
   * Creates a post with the provided data.
   * @param post The post data.
   * @param author The id of the author.
   * @return ResponseEntity
   */
  @PostMapping("")
  @Secured({"ROLE_AUTHOR"})
  public ResponseEntity<Post> postPost(
      @RequestBody Post post, @PathParam(value = "author") Integer author) {
    Optional<AppUser> optionalUser = userRepository.findById(author);
    if (optionalUser.isPresent()) {
      post.setAuthor(optionalUser.get());
      return ResponseEntity.ok(postRepository.save(post));
    }
    return ResponseEntity.notFound().build();
  }

  /**
   * Updates the post with the provided id if it exists.
   * @param post The new post data.
   * @param id The id of the post.
   * @return ResponseEntity
   */
  @PutMapping("/{id}")
  @Secured({"ROLE_AUTHOR"})
  public ResponseEntity<Post> putPost(@RequestBody Post post, @PathVariable Integer id) {
    Optional<Post> optionalPost = postRepository.findById(id);
    if (optionalPost.isPresent()) {
      Post oldPost = optionalPost.get();
      post.setId(oldPost.getId());
      post.setAuthor(oldPost.getAuthor());
      post.setComments(oldPost.getComments());
      return ResponseEntity.ok(postRepository.save(post));
    }
    return ResponseEntity.notFound().build();
  }

  /**
   * Deletes the post with the provided id if it exists.
   * @param id THe id of the post.
   * @return ResponseEntity
   */
  @DeleteMapping("/{id}")
  @Secured({"ROLE_AUTHOR"})
  public ResponseEntity deletePost(@PathVariable Integer id) {
    Optional<Post> optionalPost = postRepository.findById(id);
    if (optionalPost.isPresent()) {
      postRepository.delete(optionalPost.get());
      return ResponseEntity.ok().build();
    }
    return ResponseEntity.notFound().build();
  }
}
