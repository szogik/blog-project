package hu.elte.tools.blogbackend.controllers;

import hu.elte.tools.blogbackend.entities.AppUser;
import hu.elte.tools.blogbackend.repositories.AppUserRepository;
import hu.elte.tools.blogbackend.security.AuthenticatedUser;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * Controller class for the AppUser entity.
 */
@RequestMapping("/users")
@CrossOrigin
@RestController
public class AppUserController {

  @Autowired private AppUserRepository userRepository;

  @Autowired private BCryptPasswordEncoder passwordEncoder;

  @Autowired private AuthenticatedUser authenticatedUser;

  @GetMapping("")
  @Secured({"ROLE_AUTHOR", "ROLE_GUEST"})
  public ResponseEntity<List<AppUser>> getUsers() {
    List<AppUser> users = userRepository.findAll();
    return ResponseEntity.ok(users);
  }

  /**
   * Returns user with the provided id if exists.
   * @param id The id of the user.
   * @return ResponseEntity
   */
  @GetMapping("/{id}")
  public ResponseEntity<AppUser> getUser(@PathVariable Integer id) {
    Optional<AppUser> optionalUser = userRepository.findById(id);
    if (optionalUser.isPresent()) {
      return ResponseEntity.ok(optionalUser.get());
    }
    return ResponseEntity.notFound().build();
  }

  /**
   * Registers user with the provided data.
   * @param user The user data.
   * @return ResponseEntity
   */
  @PostMapping("/register")
  public ResponseEntity<AppUser> register(@RequestBody AppUser user) {
    Optional<AppUser> optionalAppUser = userRepository.findByUsername(user.getUsername());
    if (optionalAppUser.isPresent()) {
      return ResponseEntity.badRequest().build();
    }
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    return ResponseEntity.ok(userRepository.save(user));
  }

  /**
   * Logs in the user.
   * @return ResponseEntity
   */
  @PostMapping("login")
  public ResponseEntity login() {
    return ResponseEntity.ok(authenticatedUser.getUser());
  }

  /**
   * Updates data of the user with the provided id if it exists.
   * @param appUser The new user data.
   * @param id The id of the user.
   * @return ResponseEntity
   */
  @PutMapping("/{id}")
  @Secured({"ROLE_AUTHOR", "ROLE_GUEST"})
  public ResponseEntity<AppUser> putUser(@RequestBody AppUser appUser, @PathVariable Integer id) {
    Optional<AppUser> optionalUser = userRepository.findById(id);
    if (optionalUser.isPresent()) {
      AppUser oldUser = optionalUser.get();
      appUser.setId(oldUser.getId());
      appUser.setPosts(oldUser.getPosts());
      appUser.setComments(oldUser.getComments());
      return ResponseEntity.ok(userRepository.save(appUser));
    }
    return ResponseEntity.notFound().build();
  }

  /**
   * Deletes user with the provided id if it exists.
   * @param id The id of the user.
   * @return ResponseEntity
   */
  @DeleteMapping("/{id}")
  @Secured({"ROLE_AUTHOR"})
  public ResponseEntity deleteUser(@PathVariable Integer id) {
    Optional<AppUser> optionalUser = userRepository.findById(id);
    if (optionalUser.isPresent()) {
      userRepository.delete(optionalUser.get());
      return ResponseEntity.ok().build();
    }
    return ResponseEntity.notFound().build();
  }
}
