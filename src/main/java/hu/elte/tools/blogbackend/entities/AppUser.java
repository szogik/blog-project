package hu.elte.tools.blogbackend.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;


@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "appuser")
public class AppUser {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "username", nullable = false)
  private String username;

  @Column(name = "password", nullable = false)
  private String password;

  @Column(name = "role", nullable = false)
  @Enumerated(EnumType.STRING)
  private Role role;

  @Column(name = "email", nullable = false)
  private String email;

  @Column(name = "birth")
  private Date birth;

  @Column(name = "gender")
  private Gender gender;

  @Column(name = "website")
  private String website;

  @Column(name = "company")
  private String company;

  @OneToMany(mappedBy = "commenter", cascade = CascadeType.ALL, orphanRemoval = true)
  @JsonIgnore
  private List<Comment> comments;

  @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, orphanRemoval = true)
  @JsonIgnore
  private List<Post> posts;

  public enum Role implements GrantedAuthority {
    ROLE_GUEST,
    ROLE_AUTHOR;

    @Override
    public String getAuthority() {
      return this.toString();
    }
  }

  public enum Gender {
    MALE,
    FEMALE
  }
}
