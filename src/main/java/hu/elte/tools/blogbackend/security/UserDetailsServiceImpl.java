package hu.elte.tools.blogbackend.security;

import hu.elte.tools.blogbackend.entities.AppUser;
import hu.elte.tools.blogbackend.repositories.AppUserRepository;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

  @Autowired private AppUserRepository userRepository;

  @Autowired private AuthenticatedUser authenticatedUser;

  @Override
  @Transactional(readOnly = true)
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Optional<AppUser> optionalUser = userRepository.findByUsername(username);
    if (!optionalUser.isPresent()) {
      throw new UsernameNotFoundException(username);
    }
    AppUser user = optionalUser.get();
    authenticatedUser.setUser(user);
    Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
    grantedAuthorities.add(new SimpleGrantedAuthority(user.getRole().toString()));

    return new org.springframework.security.core.userdetails.User(
        user.getUsername(), user.getPassword(), grantedAuthorities);
  }
}
